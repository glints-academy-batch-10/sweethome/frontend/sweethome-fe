import React from 'react'
import './style/CardProject.css'
import clock from './img/Clock_small.png'
import coin from './img/Coin_small.png'
import fav from './img/Heart_small.png'
import unfav from './img/heart_unfav.png'
import search_icon from './img/search.png'

export default function CardProject() {
    return (
        <div>
            <div className="container_search">
                <div className="search">
                    <input type="seacrh" placeholder="Search..." className="search_bar" />
                    <img src={search_icon} alt="search_icon" />
                </div>
            </div>
            <div className="container_card">
                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={fav} alt="" /></button>
                    </div>

                    <div className="bot_left">
                        <a href="/detail"><h3>Natural Feel Living Room</h3></a>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>

                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

                <div className="card">
                    <div className="button_fav">
                        <button className="favorite"><img src={unfav} alt="" /></button>
                    </div>
                    <div className="bot_left">
                        <h3>Natural Feel Living Room</h3>
                        <p>CitraLand, Surabaya</p>
                    </div>
                    <div className="bot_right">
                        <span> <img src={clock} alt="clock" style={{ width: "13px" }} /> &nbsp; 2 Weeks</span>
                    </div>
                    <div className="bot_right2">
                        <span> <img src={coin} alt="clock" style={{ width: "13px" }} /> &nbsp; Rp. 70,000,000</span>
                    </div>
                </div>

            </div>

        </div>
    )
}
