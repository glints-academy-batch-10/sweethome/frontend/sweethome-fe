import './style/project.css'
import CardProject from './CardProject'
import FilterProject from './FilterProject'
import NavBar from '../../layouts/NavBar'
import Footer from '../../layouts/Footer'

const ProjectPage = () => {
	return (
		<div>
			<NavBar />
			<div className='container'>
				<div className='title'>
					<div>
						<h1>Nisi, pellentesque ullamcorper enim libero, fusce sit nulla maecenas.</h1>{' '}
					</div>
					<div>
						<h3> Nisi, pellentesque ullamcorper enim libero, fusce sit nulla maecenas.</h3>
					</div>
				</div>

				<div className='content_project'>
					<FilterProject />
					<CardProject />
				</div>

				<div className='box_pagination'>
					<button className='prev_next'>&lt;</button>
					<button className='pagination'>1</button>
					<button className='pagination'>2</button>
					<button className='pagination'>3</button>
					<button className='pagination'>4</button>
					<button className='pagination'>5</button>
					<button className='prev_next'>&gt;</button>
					<h3> </h3>
				</div>
			</div>
			<Footer />
		</div>
	)
}

export default ProjectPage
