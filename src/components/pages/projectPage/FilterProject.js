import React from 'react'
import './style/FilterProject.css'

export default function FilterProject() {
	return (
		<div className='project-filter-wrapper'>
			<div className='container_filter'>
				<h3>All Projects</h3>
			</div>
			<div className='filter'>
				<div className='filter_location'>
					<h3>Location</h3>

					<label class='checkbox'>
						<input type='checkbox' />
						<span>Living Room</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Dinning Room</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Bedroom</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Kitchen</span>
					</label>

					<label class='checkbox'>
						<input type='checkbox' />
						<span>Bathroom</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Study / Office</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Outdoor</span>
					</label>
				</div>

				<div className='filter_styles'>
					<h3>Styles</h3>

					<label class='checkbox'>
						<input type='checkbox' />
						<span>Modern</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Contemporary</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Minimalist</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Industrial</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Scandinavian</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Traditional</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Natural</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Rustic</span>
					</label>
					<label class='checkbox'>
						<input type='checkbox' />
						<span>Bohemian</span>
					</label>
				</div>
			</div>
		</div>
	)
}
