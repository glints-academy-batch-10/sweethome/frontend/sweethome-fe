//untuk cek render dashboard page
//localhost:3000/dashboard

import React, { useState } from "react";
import Breadcrumb from "../../layouts/Breadcrumb";
import BreadcrumbItem from "../../layouts/BreadcrumbItem";
import { Route, Switch } from "react-router-dom";

import Appointments from "./Appointments.js";
import Projects from "./Projects";
import Favourites from "./Favourites";
import Userinfo from "./Userinfo";
import { DashboardContainer } from "../../../assets/styles/pages/dashboardPage/index.styles";
import Tab from "../../../components/dashboardPage/Tab";
import './dashboard.css'
import NavBar from '../../layouts/NavBar'
import Footer from '../../layouts/Footer'


const DashboardPage = () => {
  const [navigation, setNavigation] = useState("appointments");

  return (
    <>
      <NavBar />
      <DashboardContainer className="dashboard">
        <Breadcrumb>
          <BreadcrumbItem name="Home" to="/" />
          <BreadcrumbItem name="Dashboard" />
        </Breadcrumb>
        <Userinfo />
        <Tab setNavigation={setNavigation} navigation={navigation} />
        {navigation === "appointments" && <Appointments />}
        {navigation === "projects" && <Projects />}
        {navigation === "favourites" && <Favourites />}
        <Switch>
          <Route
            path="/dashboardPage/appointments"
            exact
            component={() => <Appointments />}
          />
          {/* <Route path='/dashboardPage/projects' exact component ={Projects}/>
                <Route path='/dashboardPage/favourites' exact component ={Favourites}/> */}
        </Switch>
      </DashboardContainer>
      <Footer />
    </>

  );
};

export default DashboardPage;
