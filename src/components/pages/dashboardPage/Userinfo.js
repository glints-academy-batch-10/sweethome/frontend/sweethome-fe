import React from "react";
import imageUser from "../../../assets/images/Ellipse4.png";
import { UserContainer } from "../../../assets/styles/pages/dashboardPage/Userinfo.styles";
import { Link } from "react-router-dom";
import { GoPlus } from "react-icons/go";
import './dashboard.css'

export default function Userinfo() {
  return (
    <UserContainer>
      <h1 clasName="dashboard_title">Welcome Home!</h1>
      <div className="container">
        <div className="user-detail">
          <div className="pict">
            <img src={imageUser} alt="pict" />
          </div>
          <div>
            <h3>Jamal M. Koslov</h3>
            <p>jamalkoslov@gmail.com</p>
            <Link className="link">Settings</Link>
          </div>
        </div>
        <div>
          <button>
            <span className="plus">
              <GoPlus />
            </span>
            Create Appointments
          </button>
        </div>
      </div>
    </UserContainer>
  );
}
