import React from 'react'
import { Link } from 'react-router-dom'
import '../Appointment.css'

export default function EnquiryDetails() {
	return (
		<div className='container-wrapper'>
			<form className='enquiry-wrapper'>
				<div>
					<lable className='enquiry-label'>
						Building Type<span>*</span>
					</lable>{' '}
					<br />
					<select className='enquiry-select' title='select'>
						<option value='' selected disabled>
							Please select
						</option>
						<option value='house'>House</option>
						<option value='apartment'>Apartment</option>
					</select>
				</div>
				<div>
					<lable className='enquiry-label'>
						Service Type<span>*</span>
					</lable>{' '}
					<br />
					<select className='enquiry-select'>
						<option value='' selected>
							Please select
						</option>
						<option value='house'>House</option>
						<option value='apartment'>Apartment</option>
					</select>
				</div>
				<div>
					<lable className='enquiry-label'>
						Estimated Work Duration<span>*</span>
					</lable>{' '}
					<br />
					<div className='appointment-border-input'>
						<input type='text' placeholder='e.g 3' className='enquiry-input' />
						<hr />
						<span> Week(s)</span>
					</div>
				</div>
				<div>
					<lable className='enquiry-label'>
						Budget<span>*</span>
					</lable>{' '}
					<br />
					<div className='appointment-border-input'>
						<span> Rp </span> <hr />
						<input type='text' placeholder='e.g 10000000' className='enquiry-input' />
					</div>
				</div>
			</form>
			<div>
				<lable className='enquiry-label'>
					Address<span>*</span>
				</lable>{' '}
				<br />
				<textarea
					cols='175'
					rows='6'
					placeholder='e.g One East Residences 7-16&#10;Jl. Raya Kertajaya Indah No. 79&#10;Surabaya Jawa Timur'
				/>
			</div>
			<div className='filter-preferred'>
				<div className='filter-wrapper'>
					<div className='filter-title'>
						<span className='enquiry-label'>Inspection Area</span>
						<span>You may select more than one</span>
					</div>
					<div className='filter-btn-wrapper'>
						<button className='filter-btn'>Living Room</button>
						<button className='filter-btn'>Dining Room</button>
						<button className='filter-btn'>Kitchen</button>
						<button className='filter-btn'>Bedroom</button>
						<button className='filter-btn'>Bathroom</button>
						<button className='filter-btn'>Study</button>
						<button className='filter-btn'>Office</button>
						<button className='filter-btn'>Outdoor</button>
					</div>
				</div>
				<div className='filter-wrapper'>
					<div className='filter-title'>
						<span className='enquiry-label'>Preferred Style</span>
						<span>You may select more than one</span>
					</div>
					<div className='filter-btn-wrapper'>
						<button className='filter-btn'>Modern</button>
						<button className='filter-btn'>Contemporary</button>
						<button className='filter-btn'>Minimalist</button>
						<button className='filter-btn'>Industrial</button>
						<button className='filter-btn'>Scandinavian</button>
						<button className='filter-btn'>Traditional</button>
						<button className='filter-btn'>Natural</button>
						<button className='filter-btn'>Rustic</button>
						<button className='filter-btn'>Bohemian</button>
					</div>
				</div>
			</div>
			<div>
				<lable className='enquiry-label'>Note</lable> <br />
				<textarea cols='50' rows='6' placeholder='Write your request here' />
			</div>
			<div className='btn-wrapper'>
				<Link to='/appointment/appointment-date'>
					<button className='btn-submit'>Next {'  -->'}</button>
				</Link>
			</div>
		</div>
	)
}
