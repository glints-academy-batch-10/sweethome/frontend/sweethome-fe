import React from "react";
import { Link } from "react-router-dom";
import "../Appointment.css";

export default function AppointmentDate() {
  return (
    <div className="container-wrapper">
      <form className="enquiry-wrapper">
        <div>
          <label className="enquiry-label">
            Date<span>*</span>
          </label>{" "}
          <br />
          <input type="date" className="enquiry-select unstyled" />
        </div>
      </form>
      <div className="filter-date">
        <div className="filter-title">
          <span className="enquiry-label">Select appointment time</span>
        </div>
        <input className="filter-radio" type="radio" />
        <label>09.00am - 10.00am</label>
        <br />
        <input className="filter-radio" type="radio" />
        <label>10.00am - 11.00am</label>
        <br />
        <input className="filter-radio" type="radio" />
        <label>11.00am - 12.00am</label>
      </div>
      <div className="btn-wrapper">
        <Link to="/appointment/reviewccx">
          <button className="btn-submit">Next {"  -->"}</button>
        </Link>
      </div>
    </div>
  );
}
