import React from "react";
import "../Appointment.css";

export default function Review() {
  return (
    <div className="container-wrapper">
      <div className="review-wrapper">
        <div className="appointment-review">
          <p>Building Type</p>
          <p>House</p>
        </div>
        <div className="appointment-review">
          <p>Area Size</p>
          <p>160m2</p>
        </div>
        <div className="appointment-review">
          <p>Estimated Work Duration</p>
          <p>1 Month</p>
        </div>
        <div className="appointment-review">
          <p>Budget</p>
          <p>Rp. 1.000.000.000</p>
        </div>
      </div>
      <div className="appointment-review">
        <p>Address</p>
        <p>Palm Beach, Pakuwon City, Surabaya</p>
      </div>
      <div className="appointment-review">
        <p>Inspection Area</p>
        <p>Living Room and Bedroom</p>
      </div>
      <div className="appointment-review">
        <p>Date and Time</p>
        <p>1 March 2021 | 09.00 am - 10.00 am </p>
      </div>
      <div className="filter-date">
        <div className="filter-title">
          <span className="enquiry-label">Note</span>
        </div>
        <p>
          Get free professional consultation. Volutpat, risus sit augue at amet
          orci mauris viverra.
          <br /> Get free professional consultation. Volutpat, risus sit augue
          at amet orci mauris viverra.
        </p>
      </div>
      <div className="btn-wrapper">
        <button className="btn-submit" style={{ padding: "15px 30px" }}>
          <i class="fas fa-check check"></i> Create Appointment
        </button>
      </div>
    </div>
  );
}
