import './DetailProject.css'
import { useState, useEffect } from 'react'
// import { Link } from 'react-router-dom'
import Rectangle from '../../assets/images/Rectangle.png'
import Modal from 'react-modal'
import { imageDummy } from './ImageDummy'
import ImageGallery from './ImageGallery'
import NavBar from '../../layouts/NavBar'
import Footer from '../../layouts/Footer'


const DetailProject = () => {
	const [modalIsOpen, setIsOpen] = useState(false)
	const [viewImage, setViewImage] = useState('')

	const imageViewer = () => {
		if (modalIsOpen === false) {
			setViewImage('View All Images')
		} else if (modalIsOpen === true) {
			setViewImage(' ')
		}
	}

	useEffect(() => {
		imageViewer()
		// eslint-disable-next-line
	}, [modalIsOpen])
	return (
		<div>
			<NavBar />
			<Modal
				isOpen={modalIsOpen}
				onRequestClose={() => setIsOpen(false)}
				shouldCloseOnOverlayClick={false}
				style={{
					overlay: {
						backgroundColor: 'lightgray',
					},
					content: {
						backgroundColor: 'black',
						color: 'whitesmoke',
						textAlign: 'center',
					},
				}}>
				<div>
					<ImageGallery />
				</div>
			</Modal>
			<div className='detail-project-container'>
				<div className='detail-project-title'>
					<div className='detail-project-title-1'>
						<h1>Apartment Living Room Redesign</h1>
						<h3>One East Apartment, Surabaya</h3>
					</div>
					<div className='detail-project-title-2'>
						<p>Add to favourite</p>
					</div>
				</div>
				<div>
					<div class='detail-project-content-container'>
						<div class='image-area'>
							<div class='main-image'>
								<img src={imageDummy[0]} alt='main img' />
							</div>
							<div class='small-image'>
								<div className='image1'>
									<img src={imageDummy[1]} alt='img1' />
								</div>
								<div className='image2'>
									<img src={imageDummy[2]} alt='img2' />
								</div>
								<div className='all-image'>
									<h3 onClick={() => setIsOpen(true)}>{viewImage}</h3>
									<div className='all-image-layer2'>
										<img src={imageDummy[3]} alt='all-img' />
										<div className='all-image-layer1'>
											<img
												src='https://images.unsplash.com/photo-1508796079212-a4b83cbf734d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MjB8fHxlbnwwfHx8&w=1000&q=80'
												alt='background'
											/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class='description-area'>
							<div class='description-project'>
								<div>
									<h4>Property Type</h4>
									<p>Apartment</p>
									<br />
									<h4>Cost</h4>
									<p>Rp.180,000,000</p>
									<br />
									<h4>Style</h4>
									<p>Contemporary, Scandinavian and Modern</p>
								</div>
								<div>
									<h4>Area Size</h4>
									<p>
										94m<sup>2</sup>
									</p>
									<br />
									<h4>Renovation Duration</h4>
									<p>4 Weeks</p>
								</div>
							</div>
							<div class='spesification-project'>
								<h4>Work Included</h4>
								<div className='spesification-project-item'>
									<div>
										<li>Carpentery</li>
										<li>False Ceiling</li>
										<li>Flooring</li>
									</div>
									<div>
										<li>Air Conditioning</li>
										<li>Tiling</li>
										<li>Electrical Wiring</li>
									</div>
								</div>
							</div>
							<div class='quote-container'>
								<img src={Rectangle} alt='button' />
								<h4>Let our professional designer visualize your dream house</h4>
								<button>Get Free Quotes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Footer />

		</div>
	)
}

export default DetailProject
