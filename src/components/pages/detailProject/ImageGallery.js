import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { wrap } from "@popmotion/popcorn";
import { imageDummy } from "./ImageDummy";
import { BsFillCaretRightFill } from "react-icons/bs";
import { BsFillCaretLeftFill } from "react-icons/bs";
import "./ImageGallery.css";

const ImageGallery = () => {
  const [[page, direction], setPage] = useState([0, 0]);

  const imageIndex = wrap(0, imageDummy.length, page);

  const paginate = (newDirection) => {
    setPage([page + newDirection, newDirection]);
  };

  return (
    <div className="gallery-container">
      <div className="prev-btn">
        <motion.button
          whileHover={{ scale: 1.05, transition: { duration: 0.5 } }}
          whileTap={{ scale: 0.9 }}
          className="prev arrow"
          onClick={() => paginate(-1)}
        >
          <BsFillCaretLeftFill />
        </motion.button>
      </div>
      <div className="photo-container">
        <AnimatePresence initial={false} custom={direction}>
          <motion.img
            className="photo"
            key={page}
            src={imageDummy[imageIndex]}
            custom={direction}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{
              x: { type: "spring", stiffness: 300, damping: 300 },
              opacity: { duration: 1 },
            }}
            drag="x"
            dragConstraints={{ left: 0, right: 0 }}
            dragElastic={1}
          />
        </AnimatePresence>
      </div>
      <div className="next-btn">
        <motion.button
          whileHover={{ scale: 1.05, transition: { duration: 0.5 } }}
          whileTap={{ scale: 0.9 }}
          className="next arrow"
          onClick={() => paginate(1)}
        >
          <BsFillCaretRightFill />
        </motion.button>
      </div>
    </div>
  );
};

export default ImageGallery;
