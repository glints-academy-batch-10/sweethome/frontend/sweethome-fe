import React, { useState } from "react";
import "./AdminCreateShowcase.scss";
import img_browser from "./img/Gallery.png";
import x_icon from './img/Cross 16px.png'
import prof_pic from './img/prof-pic.jpg'
import { VscChevronRight } from "react-icons/vsc";


export default function AdminCreateShowcase() {
    const [imageData, setImageData] = useState(null); // ini fungsinya untuk dimasukin ke body
    const [imageURL, setImageURL] = useState(null); // ini fungsinya untuk mendisplay image setelah di select dari storage
    //data lain2
    // const [workInclude, setworkInclude] = useState(null);
    // const [style, setstyle] = useState(null);

    const UploadImage = () => {
        const data = new FormData();
        data.append("file", imageData);

        /**
         * pura-pura ngefetch
         */
        // API.post("/upload-showcase/image", {
        //   dataImage: imageData,
        // });
    };

    const UploadForm = () => {
        /**
         * pura-pura ngefetch
         */
        // API.post("/upload-showcase/form", {
        //   data1: workInclude,
        //   data2: style,
        // });
    };

    const onCreate = () => {
        UploadForm().then();
        UploadImage().then();
        // history.push("/");
    };

    console.log("imageData", imageData);
    console.log("imageURL", imageURL);

    return (
        <div className="create_showcase_body">
            <div className="create_showcase_container">
                <h3>Create Showcase</h3>
                <div className="showcase_breadcrumb">
                    <a href="/admin/overview">Home</a>
                    <VscChevronRight style={{
                        margin: "0 0.5rem",
                        color: "#828282",
                    }} />
                    <a href="/admin/showcase">Showcase</a>
                    <VscChevronRight style={{
                        margin: "0 0.5rem",
                        color: "#828282",
                    }} />
                    <a href="/admin/create-showcase" style={{ fontWeight: "bold", color: "#214457" }}> Create Showcase</a>
                </div>

                <div className="create_showcase_content">
                    <div className="create_showcase_content_1">
                        <div>
                            <label className="label_showcase">
                                Showcase Type<span className="span_showcase">*</span>
                            </label>
                            <select className="select_showcase_type_1">
                                <option value="" selected>
                                    Please Select
                                 </option>
                                <option value="">Portofolio</option>
                                <option value="">Completed Prrject</option>
                            </select>
                        </div>
                        <div>
                            <label className="label_showcase">
                                Project<span className="span_showcase">*</span>
                            </label>
                            <select className="select_showcase_type_1">
                                <option value="" selected>
                                    Please Select
                                 </option>
                                <option value="">Portofolio</option>
                                <option value="">Completed Prrject</option>
                            </select>
                        </div>
                    </div>

                    <div className="create_showcase_content_2">
                        <div>
                            <div>
                                <img src={prof_pic} alt="prof_pic" />
                            </div>
                            <div className="showcase_profpic_detail">
                                <div>
                                    <span>Name</span>
                                    <p>Yedam Mansuri</p>
                                </div>
                                <div>
                                    <span>Phone</span>
                                    <p>+62 8111 1111 1111</p>
                                </div>
                                <div>
                                    <span>Email</span>
                                    <p>stellahobart@gmail.com</p>
                                </div>
                            </div>
                        </div>
                        <div className="showcase_detail_project">
                            <div>
                                <span>Total Price</span>
                                <p>Rp.57.000.000</p>
                            </div>
                            <div>
                                <span>Address</span>
                                <p>Palm Beach, Pakuwon City, Surabaya</p>
                            </div>
                            <div>
                                <span>Related Appointment</span>
                                <p style={{ textDecoration: "underline" }}>#A-0001</p>
                            </div>
                            <div>
                                <span>Total Duration</span>
                                <p>1 Month</p>
                            </div>
                        </div>
                    </div>

                    <div className="create_showcase_content_3">
                        <div>
                            <label className="label_showcase">
                                Work Included<span className="span_showcase">*</span>
                            </label>
                            <select className="select_showcase_type_2">
                                <option value="" selected>
                                    Please Select
                                 </option>
                                <option value="">Portofolio</option>
                                <option value="">Completed Prrject</option>
                            </select>
                            <p>Status</p>
                        </div>
                        <div>
                            <label className="label_showcase">
                                Styles<span className="span_showcase">*</span>
                            </label>
                            <select className="select_showcase_type_2">
                                <option value="" selected>
                                    Please Select
                             </option>
                                <option value="">Portofolio</option>
                                <option value="">Completed Prrject</option>
                            </select>
                        </div>
                    </div>

                    <div className="create_showcase_content_4">
                        <label className="label_showcase">Gallery</label>

                        {!imageURL ? (
                            <div>
                                <input
                                    type="file"
                                    id="upload"
                                    hidden
                                    onChange={(event) => {
                                        setImageData(event.target.files[0]);
                                        setImageURL(URL.createObjectURL(event.target.files[0]));
                                    }}
                                />
                                <label className="custom_upload_file" for="upload">
                                    <img src={img_browser} alt="img_browser"></img>
                                    <p>Upload Image</p>
                                </label>
                            </div>
                        ) : (
                                <div>

                                    <div className="custom_upload_file--selected">
                                        <div>
                                            <img src={imageURL} alt="" />
                                            <button onClick={() => setImageURL(null)}> <img src={x_icon} alt="" /></button>

                                            <input
                                                type="file"
                                                id="upload"
                                                hidden
                                                onChange={(event) => {
                                                    setImageData(event.target.files[0]);
                                                    setImageURL(URL.createObjectURL(event.target.files[0]));
                                                }}
                                            />
                                            <label className="label_showcase">
                                                Title<span className="span_showcase">*</span>
                                            </label>
                                            <input type="text" name="" id="" />
                                        </div>
                                    </div>

                                    <label className="custom_upload_file" for="upload">
                                        <img src={img_browser} alt="img_browser"></img>
                                        <p>Upload Image</p>
                                    </label>

                                </div>

                            )}
                    </div>

                </div>
                <div className="box_for_btn_create_showcase">
                    <a href="/admin/showcase">
                        <button onClick={onCreate}> Create Showcase</button>
                    </a>
                </div>
            </div>
        </div>
    );
}
