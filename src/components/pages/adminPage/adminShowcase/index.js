import React from 'react'
import './adminShowcase.scss'
import search_icon from '../../projectPage/img/search.png'
import sort_icon from './img/sort_icon.png'
import filter_icon from './img/filter_icon.png'
import points_icon from './img/3point_icon.png'
import arrow_left_icon from './img/arrow - left.png'
import arrow_right_icon from './img/arrow - right.png'

export default function adminShowcase() {
	return (
		<div className='showcase_background'>
			<div className='showcase_container'>
				<div className='showcase_header'>
					<h3>Show Case</h3>
					<div className='showcase_box_search'>
						<input type='search' className='showcase_search' placeholder='Search..' />
						<img src={search_icon} alt='search_icon' />
					</div>
				</div>

				<div className='showcase_content'>
					<div className='showcase_btn_srt_fltr'>
						<a href='/admin/create-showcase'>
							<button className='btn_create_showcase'> + &nbsp; &nbsp; Create New</button>
						</a>
						<div className='showcase_srt_btn'>
							<img src={sort_icon} alt='' />
							<span>Sort</span>
						</div>
						<div className='showcase_fltr_btn'>
							<img src={filter_icon} alt='' />
							<span>Filter</span>
						</div>
					</div>

					<div className='showcase_title_detail'>
						<span>Name</span>
						<span>Type</span>
						<span>Created</span>
						<span>Showcase</span>
					</div>

					<div className='showcase_field_detail'>
						<div>
							<a href='/detail' target='_blank'>
								{' '}
								Apartment Living Room Redesign
							</a>
							<p>Created at 20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<span> Completed Project</span>
							<p>#P-0001</p>
						</div>
						<div>
							<span>Justin Junaedy</span>
							<p>20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<input type='checkbox' id='switch' />
							<label for='switch'></label>
						</div>
						<div>
							<img src={points_icon} alt='3 points' />
						</div>
					</div>

					<div className='showcase_field_detail'>
						<div>
							<a href='/detail' target='_blank'>
								{' '}
								Apartment Living Room Redesign
							</a>
							<p>Created at 20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<span>Portfolio</span>
							<p></p>
						</div>
						<div>
							<span>Justin Junaedy</span>
							<p>20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<input type='checkbox' id='switch1' />
							<label for='switch1'></label>
						</div>
						<div>
							<img src={points_icon} alt='3 points' />
						</div>
					</div>

					<div className='showcase_field_detail'>
						<div>
							<a href='/detail' target='_blank'>
								{' '}
								Apartment Living Room Redesign
							</a>
							<p>Created at 20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<span> Completed Project</span>
							<p>#P-0001</p>
						</div>
						<div>
							<span>Justin Junaedy</span>
							<p>20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<input type='checkbox' id='switch' />
							<label for='switch'></label>
						</div>
						<div>
							<img src={points_icon} alt='3 points' />
						</div>
					</div>

					<div className='showcase_field_detail'>
						<div>
							<a href='/detail' target='_blank'>
								{' '}
								Apartment Living Room Redesign
							</a>
							<p>Created at 20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<span>Portfolio</span>
							<p></p>
						</div>
						<div>
							<span>Justin Junaedy</span>
							<p>20 January 2021, 9:45 PM</p>
						</div>
						<div>
							<input type='checkbox' id='switch1' />
							<label for='switch1'></label>
						</div>
						<div>
							<img src={points_icon} alt='3 points' />
						</div>
					</div>

					<div className='showcase_pagination'>
						<div>
							<p>Rows per page:</p>
							<span>8</span>
							<p>1-8 of 1240</p>
							<button>
								{' '}
								<img src={arrow_left_icon} alt='arrow_left_icon' />{' '}
							</button>
							<button>
								{' '}
								<img src={arrow_right_icon} alt='arrow_right_icon' />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
