import './style.css'
import { Line } from 'react-chartjs-2'
import SweetHomeLogo from '../../../layouts/assets/SweetHome.png'

const AdminOverview = () => {
	const month = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	const today = new Date(),
		date = today.getDate() + ' ' + month[today.getMonth() + 1] + ' ' + today.getFullYear()

	return (
		<div className='overview-container'>
			<div className='admin-navbar'>
				<img src={SweetHomeLogo} alt="SweetHomeLogo" className='navbar-logo' />
				<button className='col-2-border'>Logout</button>
			</div>
			<div className='upcoming-appointment-container'>
				<h2 className='overview-title'>Upcoming Appointment</h2>
				<div className='upcoming-appointment-date'>
					<div className='upcoming-appointment-date-1'>
						<h3 className='upcoming-appointment-date-todaytitle'>TODAY - {date}</h3>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
					</div>
					<div className='upcoming-appointment-date-2'>
						<h3 className='upcoming-appointment-date-title'>{date}</h3>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
					</div>
					<div className='upcoming-appointment-date-3'>
						<h3 className='upcoming-appointment-date-title'>{date}</h3>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
						<h4>
							<span>10AM - 11AM</span> Travis Sucipto
						</h4>
						<p>Interior Design | Kitchen</p>
					</div>
				</div>
			</div>
			<div className='chart-oveview-container'>
				<h2 className='overview-title'>This Years’s trends</h2>
				<div className='chart-overview-container'>
					<div className='chart-data'>
						<Line
							data={{
								labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
								datasets: [
									{
										label: 'Appointment',
										data: [5, 6, 7, 5, 4, 3, 7, 8, 9, 6, 7, 8],
										borderColor: ['rgba(33, 68, 87, 1)'],
										fill: [false],
										pointHoverBackgroundColor: ['rgba(66, 192, 215, 1)'],
									},
									{
										label: 'Order',
										data: [6, 5, 2, 5, 4, 6, 7, 5, 8, 5, 6, 5],
										borderColor: ['rgba(242, 213, 85, 1)'],
										fill: [false],
										pointHoverBackgroundColor: ['rgba(66, 192, 215, 1)'],
									},
								],
							}}
						/>
					</div>
					<div className='chart-detail'>
						<h4>Created</h4>
						<p>449</p>
						<h4>On Going</h4>
						<p>426</p>
						<h4>Finished</h4>
						<p>33</p>
						<h4>Cancelled</h4>
						<p>3</p>
						<h4>Average Project Completion</h4>
						<p>2 Month</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default AdminOverview
