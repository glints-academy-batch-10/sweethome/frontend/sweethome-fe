import React from "react";
import photo from "../photo.svg";
import "../AdminAppointment.css";

export default function AppointmentListDetail() {
  return (
    <div className="setting-container">
      <div className="setting-wrapper">
        <div className="adm-appointment-wrapper">
          <div className="adm-appointment-detail">
            <div>
              <img src={photo} alt="imgclear" className="adm-appointment-detail-img" />
            </div>
            <div>
              <p>Name</p>
              <p>Yedam Mansuri</p>
              <p>Phone</p>
              <p>+62 111 111 111</p>
              <p>Email</p>
              <p>yedamansuri@gmail.com</p>
            </div>
          </div>
          <hr />
          <div className="adm-appointment-detail-content">
            <div>
              <p>Building Type</p>
              <p>House</p>
            </div>
            <div>
              <p>Service Type</p>
              <p>Interior Design</p>
            </div>
            <div>
              <p>Budget</p>
              <p>Rp. 1.000.000.000</p>
            </div>
            <div>
              <p>Estimated Duration</p>
              <p>1 Month</p>
            </div>
            <div>
              <p>Address</p>
              <p>Palm Beach, Pakuwon City, Surabaya</p>
            </div>
          </div>
        </div>
      </div>
      <div className="setting-wrapper adm-appointment-space">
        <div className="adm-appointment-detail-list">
          <div>
            <p>Inspection Area</p>
            <ul>
              <li>Living Room</li>
              <li>Kitchen</li>
              <li>Bedroom</li>
            </ul>
          </div>
          <div>
            <p>Preffered Style</p>
            <ul>
              <li>Living Room</li>
              <li>Kitchen</li>
              <li>Bedroom</li>
            </ul>
          </div>
          <div>
            <p>Note</p>
            <p>
              Odio tortor tincidunt risus aliquet malesuada semper. Vestibulum
              id faucibus aliquam ut bibendum praesent nibh. Fames sed pulvinar
              sagittis non leo tortor, quis. Volutpat nec diam ac venenatis
              commodo scelerisque arcu venenatis. Sem euismod urna ac egestas
              dignissim eros.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
