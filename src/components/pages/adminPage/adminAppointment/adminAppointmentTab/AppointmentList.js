import React from "react";
import { Link } from "react-router-dom";
import { FaEllipsisV } from "react-icons/fa";
import "../AdminAppointment.css";

export default function AppointmentList() {
  return (
    <table className="setting-table">
      <thead className="setting-adm-table">
        <tr>
          <td>Customer</td>
          <td>Inquiry</td>

          <td>Appointment Date</td>
          <td>Status</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Travis Sucipto</td>
          <td>Living Room</td>
          <td>28 Januari 2021</td>
          <td>
            <span className="adm-appointment-status">SCHECULED</span>
          </td>
          <td>
            <Link to="/admin/appointment/list-detail">
              <button className="setting-btn-icon">
                <FaEllipsisV className="setting-table-icon" />
              </button>
            </Link>
          </td>
        </tr>
        <tr>
          <td>Stella Hobart</td>
          <td>Kitchen</td>
          <td>28 Januari 2021</td>
          <td>
            <span className="adm-appointment-status">DECLINED</span>
          </td>
          <td>
            <button className="setting-btn-icon">
              <FaEllipsisV className="setting-table-icon" />
            </button>
          </td>
        </tr>
        <tr>
          <td>Mullet Subekti</td>
          <td>Living Room</td>
          <td>28 Januari 2021</td>
          <td>
            <span className="adm-appointment-status">DONE</span>
          </td>
          <td>
            <button className="setting-btn-icon">
              <FaEllipsisV className="setting-table-icon" />
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  );
}
