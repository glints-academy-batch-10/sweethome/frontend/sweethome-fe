import React from 'react'
import './adminProject.css'
import { AiOutlinePlus } from 'react-icons/ai'
import { BiSortUp } from 'react-icons/bi'
import { FaFilter } from 'react-icons/fa'
import { Link } from 'react-router-dom'

export default function adminProject() {
	return (
		<div className='adm-project'>
			<div className='project-list-head'>
				<h3>Orders</h3>
				<form action=''>
					<input type='text' placeholder='Search..' className='pl-searchbar' />
				</form>
			</div>
			<div className='project-list'>
				<div className='pl-top'>
					<Link to='/admin/project/new' style={{ marginLeft: '3.5%', marginTop: '2.5%' }}>
						<button className='pl-btn-new'>
							<AiOutlinePlus
								style={{
									verticalAlign: 'middle',
									marginRight: '5%',
									fontSize: '18px',
								}}
							/>{' '}
							Create New
						</button>
					</Link>
					<div className='filter-sort'>
						<p className='fs-sort'>
							<BiSortUp
								style={{
									verticalAlign: 'middle',
									marginRight: '15%',
									fontSize: '18px',
									color: '#C5C7CD',
								}}
							/>
							Sort
						</p>
						<p className='fs-filter'>
							<FaFilter
								style={{
									verticalAlign: 'middle',
									marginRight: '15%',
									fontSize: '14px',
									color: '#C5C7CD',
								}}
							/>
							Filter
						</p>
					</div>
				</div>
				<div className='pl-titles'>
					<p style={{ paddingRight: '23.5%' }}>Customer</p>
					<p style={{ paddingRight: '11%' }}>Package</p>
					<p style={{ paddingRight: '12.25%' }}>Price</p>
					<p style={{ paddingRight: '6%' }}>Est. Completion Date</p>
					<p>Status</p>
				</div>
				<hr className='pl-hr' />
			</div>
		</div>
	)
}
