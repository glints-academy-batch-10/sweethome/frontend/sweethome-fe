import React from "react";
import "./newProject.css";
import { VscChevronRight } from "react-icons/vsc";

const newProject = () => {
  return (
    <div className="adm-project-new">
      <h3>Create Order</h3>
      <p className="adm-new-breadcrumb">
        <a href="/" style={{ textDecoration: "none", color: "#828282" }}>
          Home
        </a>{" "}
        <VscChevronRight
          style={{
            paddingTop: "10px",
            marginRight: "0.5%",
            fontSize: "18px",
            color: "#828282",
          }}
        />
        <a href="/admin/project" className="adm-p-link">
          Order
        </a>{" "}
        <VscChevronRight
          style={{
            verticalAlign: "middle",
            marginRight: "0.5%",
            fontSize: "18px",
            color: "#828282",
          }}
        />
        <span style={{ color: "#214457", fontWeight: "bold" }}>
          Create Order
        </span>
      </p>
      <div className="project-cust-select">
        <div className="cust-select">
          <p>Customer Details</p>
          <select name="" id="">
            <option value="">Please select...</option>
          </select>
        </div>
        <div className="cust-select">
          <p>Appointment Details</p>
          <select name="" id="">
            <option value="">Please select...</option>
          </select>
        </div>
      </div>
      <div className="project-detail-select">
        <p style={{ fontWeight: "bold" }}>Project Details</p>
        <hr
          style={{
            border: "1px solid #999999",
            height: "0px",
            marginTop: "1.5%",
          }}
        />
        <div className="loc-type">
          <div className="loc-select">
            <p style={{ fontWeight: "bold", fontSize: "14px" }}>
              Location<span style={{ color: "#DD5571" }}>*</span>
            </p>
            <select name="" id="">
              <option value="">Please select...</option>
            </select>
          </div>
          <div className="pro-type">
            <p style={{ fontWeight: "bold", fontSize: "14px" }}>
              Project Type<span style={{ color: "#DD5571" }}>*</span>
            </p>
            <select name="" id="">
              <option value="">Please select...</option>
            </select>
          </div>
        </div>
        <div className="work-area-price">
          <div className="work-area">
            <div>
              <p style={{ fontWeight: "bold", fontSize: "14px" }}>
                Work Duration (weeks)<span style={{ color: "#DD5571" }}>*</span>
              </p>
              <input type="text" placeholder="e.g 2" />
            </div>
            <div>
              <p style={{ fontWeight: "bold", fontSize: "14px" }}>
                Area (m2)<span style={{ color: "#DD5571" }}>*</span>
              </p>
              <input type="text" placeholder="e.g 30" />
            </div>
          </div>
          <div className="wap-price">
            <p style={{ fontWeight: "bold", fontSize: "14px" }}>
              Price (Rp)<span style={{ color: "#DD5571" }}>*</span>
            </p>
            <input type="text" placeholder="e.g 10,000,000" />
          </div>
        </div>
        <button className="new-pkg-btn">New Package</button>
      </div>
      <div className="duration-total">
        <div className="dt-dur">
          <p
            style={{ fontWeight: "bold", fontSize: "16px", marginRight: "5%" }}
          >
            Duration
          </p>
          <p style={{ fontSize: "16px", width: "6rem" }}>0 Week(s)</p>
        </div>
        <div className="dt-total">
          <p
            style={{ fontWeight: "bold", fontSize: "16px", marginRight: "5%" }}
          >
            Total
          </p>
          <p style={{ fontSize: "16px", width: "8rem" }}>Rp 0</p>
        </div>
      </div>
      <div className="create-order-btn-div">
        <button className="btn-create-order">Create Order</button>
      </div>
    </div>
  );
};

export default newProject;
