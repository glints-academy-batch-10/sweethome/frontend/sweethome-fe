import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Modal from 'react-modal'
// import Table from '../Tables'
import '../../../Setting.css'
import { FaSortAmountUp, FaFilter, FaEllipsisV } from 'react-icons/fa'
import { AiOutlinePlus, AiOutlineClose } from 'react-icons/ai'

export default function AdminList() {
	const [isModalOpen, setIsModalOpen] = useState(false)
	return (
		<div className='setting-container'>
			<p>Admin</p>
			<div className='setting-wrapper'>
				<div className='setting-head'>
					<div>
						<button
							className='setting-btn-submit'
							onClick={() => {
								setIsModalOpen(true)
							}}>
							<AiOutlinePlus className='setting-plus-icon' /> Create New
						</button>
					</div>
					<div className='setting-btn-timeslot'>
						<button className='setting-btn-icon'>
							<FaSortAmountUp className='setting-table-icon' />
							<span>Sort</span>
						</button>
						<button className='setting-btn-icon'>
							<FaFilter className='setting-table-icon' />
							<span>Filter</span>
						</button>
					</div>
				</div>
				<table className='setting-table'>
					<thead className='setting-table-head'>
						<tr>
							<td>Name</td>
							<td>Email</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Justin Junaedi</td>
							<td>justinjunaedi@gmail.com</td>
							<td>
								<Link to='/admin/setting/admin-list'>
									<button className='setting-btn-icon'>
										<FaEllipsisV className='setting-table-icon' />
									</button>
								</Link>
							</td>
						</tr>
						<tr>
							<td>Renata Yoga</td>
							<td>renatayoga@gmail.com</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<Modal
				isOpen={isModalOpen}
				onRequestClose={() => setIsModalOpen(false)}
				className='setting-modal-container'
				overlayClassName='setting-modal-overlay'
				contentLabel='Learn Modal'>
				<div className='setting-modal-wrapper'>
					<div className='setting-modal-content'>
						<p>Create New Admin</p>
						<button
							onClick={() => {
								setIsModalOpen(false)
							}}>
							<AiOutlineClose />
						</button>
					</div>
					<form className='setting-modal-form'>
						<div className='setting-modal-timeslot'>
							<div>
								<label>
									Name<span>*</span>
								</label>
								<br />
								<input type='text' placeholder='e.g Justin Junaedi' />
							</div>
							<div>
								<label>
									Email<span>*</span>
								</label>
								<br />
								<input type='text' placeholder='e.g justinjunaedi@gmail.com' />
							</div>
						</div>
						<label>
							Password<span>*</span>
						</label>
						<br />
						<input className='setting-input-content' type='password' placeholder='********' />
						<div className='setting-btn-modal-wrapper'>
							<button className='setting-btn-modal'>Create Admin</button>
						</div>
					</form>
				</div>
			</Modal>
		</div>
	)
}
