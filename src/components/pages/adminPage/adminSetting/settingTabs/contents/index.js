import React, { useState } from 'react'
// import { Switch, Route, NavLink } from "react-router-dom";
import Modal from 'react-modal'
// import Location from "./contentTabs/Location";
// import BuildingType from "./contentTabs/BuildingType";
// import ServiceType from "./contentTabs/ServiceType";
// import ProjectType from "./contentTabs/ProjectType";
// import Styles from "./contentTabs/Styles";
import { AiOutlinePlus, AiOutlineClose } from 'react-icons/ai'
import { FaEllipsisV } from 'react-icons/fa'
import '../../Setting.css'

export default function Content() {
	const [isModalOpen, setIsModalOpen] = useState(false)

	return (
		<div className='setting-container'>
			<p>Contents</p>
			<div className='setting-wrapper'>
				<div className='setting-head'>
					<div>
						<button className='setting-content-btn'>Location</button>
						<button className='setting-content-btn'>Building Type</button>
						<button className='setting-content-btn'>Service Type</button>
						<button className='setting-content-btn'>Project Type</button>
						<button className='setting-content-btn'>Styles</button>

						{/* <NavLink
              className="setting-content-btn"
              activeClassName="setting-content-btn-active"
              exact
              to="/admin/setting"
            >
              Location
            </NavLink>
            <NavLink
              exact
              className="setting-content-btn"
              activeClassName="setting-content-btn-active"
              to="/admin/setting/content/building-type"
            >
              Building Type
            </NavLink>
            <NavLink
              exact
              className="setting-content-btn"
              activeClassName="setting-content-btn-active"
              to="/admin/setting/content/service-type"
            >
              Service Type
            </NavLink>
            <NavLink
              exact
              className="setting-content-btn"
              activeClassName="setting-content-btn-active"
              to="/admin/setting/content/project-type"
            >
              Project Type
            </NavLink>
            <NavLink
              exact
              className="setting-content-btn"
              activeClassName="setting-content-btn-active"
              to="/admin/setting/content/styles"
            >
              Styles
            </NavLink> */}
					</div>
					<div>
						<button
							className='setting-btn-submit'
							onClick={() => {
								setIsModalOpen(true)
							}}>
							<AiOutlinePlus className='setting-plus-icon' /> Create New
						</button>
					</div>
				</div>
				<table className='setting-table'>
					<thead className='setting-table-head'>
						<tr>
							<td>Name</td>
							<td>Created At</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Living Room</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
						<tr>
							<td>Dining Room</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
						<tr>
							<td>Bedroom</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
					</tbody>
				</table>
				{/* <Switch>
          <Route
            exact
            path="/admin/setting/content/building-type"
            component={BuildingType}
          />
          <Route
            exact
            path="/admin/setting/content/service-type"
            component={ServiceType}
          />
          <Route
            exact
            path="/admin/setting/content/project-type"
            component={ProjectType}
          />
          <Route
            exact
            path="/admin/setting/content/styles"
            component={Styles}
          />
          <Route exact path="/admin/setting" component={Location} />
        </Switch> */}
			</div>
			<Modal
				isOpen={isModalOpen}
				onRequestClose={() => setIsModalOpen(false)}
				className='setting-modal-container'
				overlayClassName='setting-modal-overlay'
				contentLabel='Learn Modal'>
				<div className='setting-modal-wrapper'>
					<div className='setting-modal-content'>
						<p>Create New Content</p>
						<button
							onClick={() => {
								setIsModalOpen(false)
							}}>
							<AiOutlineClose />
						</button>
					</div>
					<form className='setting-modal-form'>
						<label>
							Category<span>*</span>
						</label>
						<br />
						<input type='text' /> <br />
						<label>
							Content<span>*</span>
						</label>
						<br />
						<input className='setting-input-content' type='text' />
						<div className='setting-btn-modal-wrapper'>
							<button className='setting-btn-modal'>Create Content</button>
						</div>
					</form>
				</div>
			</Modal>
		</div>
	)
}
