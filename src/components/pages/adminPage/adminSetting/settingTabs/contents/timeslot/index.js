import React from 'react'
import { Link } from 'react-router-dom'
// import Table from '../Tables'
import '../../../Setting.css'
import { FaSortAmountUp, FaFilter, FaEllipsisV } from 'react-icons/fa'

export default function Timeslot() {
	return (
		<div className='setting-container'>
			<p>Timeslot</p>
			<div className='setting-wrapper'>
				<div className='setting-btn-timeslot'>
					<button className='setting-btn-icon'>
						<FaSortAmountUp className='setting-table-icon' />
						<span>Sort</span>
					</button>
					<button className='setting-btn-icon'>
						<FaFilter className='setting-table-icon' />
						<span>Filter</span>
					</button>
				</div>
				<table className='setting-table'>
					<thead className='setting-table-head'>
						<tr>
							<td>Service Type</td>
							<td>Updated At</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Renovation</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<Link to='/admin/setting/timeslot/renovation'>
									<button className='setting-btn-icon'>
										<FaEllipsisV className='setting-table-icon' />
									</button>
								</Link>
							</td>
						</tr>
						<tr>
							<td>Interior Design</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
						<tr>
							<td>Building Inspection</td>
							<td>28 January 2021, 09:00 AM</td>
							<td>
								<button className='setting-btn-icon'>
									<FaEllipsisV className='setting-table-icon' />
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	)
}
