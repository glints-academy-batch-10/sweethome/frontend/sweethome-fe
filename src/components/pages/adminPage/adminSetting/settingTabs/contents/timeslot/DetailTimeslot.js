import React, { useState } from "react";
import Modal from "react-modal";
// import Table from "../Tables";
import "../../../Setting.css";
import { /*FaSortAmountUp, FaFilter,*/ FaEllipsisV } from "react-icons/fa";
import { AiOutlinePlus, AiOutlineClose } from "react-icons/ai";

export default function DetailTimeslot() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <div className="setting-container">
      <div className="setting-wrapper">
        <div className="setting-head">
          <p>Renovation Timeslot</p>
          <div>
            <button
              className="setting-btn-submit"
              onClick={() => {
                setIsModalOpen(true);
              }}
            >
              <AiOutlinePlus className="setting-plus-icon" /> Create New
            </button>
          </div>
        </div>
        <table className="setting-table">
          <thead className="setting-table-head">
            <tr>
              <td>Timeslot</td>
              <td>Quota</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>08:00 AM - 09:00 AM</td>
              <td>3</td>
              <td>
                <button className="setting-btn-icon">
                  <FaEllipsisV className="setting-table-icon" />
                </button>
              </td>
            </tr>
            <tr>
              <td>09:00 AM - 10:00 AM</td>
              <td>3</td>
              <td>
                <button className="setting-btn-icon">
                  <FaEllipsisV className="setting-table-icon" />
                </button>
              </td>
            </tr>
            <tr>
              <td>11:00 AM - 12:00 PM</td>
              <td>2</td>
              <td>
                <button className="setting-btn-icon">
                  <FaEllipsisV className="setting-table-icon" />
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        className="setting-modal-container"
        overlayClassName="setting-modal-overlay"
        contentLabel="Learn Modal"
      >
        <div className="setting-modal-wrapper">
          <div className="setting-modal-content">
            <p>Create New Timeslot</p>
            <button
              onClick={() => {
                setIsModalOpen(false);
              }}
            >
              <AiOutlineClose />
            </button>
          </div>
          <form className="setting-modal-form">
            <div className="setting-modal-timeslot">
              <div>
                <label>
                  Start<span>*</span>
                </label>
                <br />
                <input type="text" />
              </div>
              <div>
                <label>
                  End<span>*</span>
                </label>
                <br />
                <input type="text" />
              </div>
            </div>
            <label>
              Quota<span>*</span>
            </label>
            <br />
            <input className="setting-input-content" type="text" />
            <div className="setting-btn-modal-wrapper">
              <button className="setting-btn-modal">Create Timeslot</button>
            </div>
          </form>
        </div>
      </Modal>
    </div>
  );
}
