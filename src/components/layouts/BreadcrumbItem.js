import React from "react";
import { Link } from "react-router-dom";
import { IoIosArrowForward } from "react-icons/io";
import { CgHome } from "react-icons/cg";
import {
  Container,
  BreadcrumbName,
} from "../../assets/styles/components/layout/Breadcrumbitem.styles";

export default function BreadcrumbItem(props) {
  return !props.to ? (
    <BreadcrumbName className="breadcrumb-name">{props.name}</BreadcrumbName>
  ) : (
    <Container>
      <span className="logo">
        <CgHome link to="/" />
      </span>
      <Link className="link" to={props.to}>
        {props.name}
      </Link>
      <span className="arrow">
        <IoIosArrowForward />
      </span>
    </Container>
  );
}
