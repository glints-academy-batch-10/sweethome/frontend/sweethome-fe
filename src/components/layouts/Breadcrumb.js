import React from "react";
import { Container } from "../../assets/styles/components/layout/Breadcrumb.styles";

export default function Breadcrumb(props) {
  return <Container className="breadcrumb">{props.children}</Container>;
}
