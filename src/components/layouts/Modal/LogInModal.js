import React, { useRef, /*useEffect,*/ useState } from 'react'
import background from '../../pages/homePage/assets/background.png'
import './LogInModal.css'
import { VscClose } from 'react-icons/vsc'
import { FaGoogle } from 'react-icons/fa'
import { ImFacebook } from 'react-icons/im'
import { useSelector, useDispatch } from 'react-redux'
import { postLogin } from '../../../redux/action/Login'

const LogInModal = ({ showModal, setShowModal }) => {
	//* --------START LOGIN SETUP----------
	const dispatch = useDispatch()
	const logData = useSelector((state) => state.login.data)

	const [loginData, setLoginData] = useState({
		email: '',
		password: '',
	})

	const handleChangeLogin = (e) => {
		setLoginData({
			...loginData,
			[e.target.name]: e.target.value,
		})
	}

	const handleSubmitLogin = (e) => {
		e.preventDefault()
		dispatch(postLogin(loginData))
	}

	// const handleLogOut = () => {
	// 	localStorage.clear()
	// }

	console.log(loginData)
	console.log(logData)
	//* ---------END LOGIN SETUP-----------

	const modalRef = useRef()
	const closeModal = (e) => {
		if (modalRef.current === e.target) {
			setShowModal(false)
		}
	}
	return (
		<div>
			{showModal ? (
				<div className='modal-background' onClick={closeModal} ref={modalRef}>
					<div className='modal-container'>
						<img src={background} alt='background' className='modal-img' />
						<h4 className='login-welcome'>Welcome Back!</h4>
						<p className='login-p'>Rhoncus sed at nulla odio. Faucibus quam magna feugiat vitae in. Risus et fermentum in risus nibh praesent netus bibendum</p>

						<div className='modal-content'>
							<VscClose
								style={{
									cursor: 'pointer',
									position: 'absolute',
									top: '20px',
									right: '20px',
									width: '32px',
									height: '32px',
									padding: 0,
									'z-index': 10,
									color: '#373737',
								}}
								onClick={() => setShowModal((prev) => !prev)}
							/>
							<h4>Login</h4>

							{/* <h5 className='cross'>&#10005;</h5> */}

							<p>
								Don’t have an account? <span>Signup</span>
							</p>
							<form onChange={handleChangeLogin} onSubmit={handleSubmitLogin}>
								<label className='modal-email'>Email</label>
								<input name='email' type='email' placeholder='e.g justinjunaedi@gmail.com' className='email-input' />
								<label className='modal-password'>Password</label>
								<input name='password' type='password' placeholder='&#42;&#42;&#42;&#42;&#42;&#42;&#42;&#42;' className='password-input' />
								<button className='login-button'>Login</button>
							</form>
							<hr className='line-hr' />
							<div className='google-button'>
								<button className='google-btn'>
									<FaGoogle
										style={{
											margin: 0,
											top: '14.03px',
											position: 'absolute',
											left: '38.17px',
											width: '13.67px',
											height: '13.67px',
											marginRight: '20px',
										}}
									/>
									&nbsp; &nbsp; &nbsp; &nbsp;Google
								</button>
							</div>
							<div className='facebook-button'>
								<button className='facebook-btn'>
									<ImFacebook
										style={{
											margin: 0,
											top: '14.03px',
											position: 'absolute',
											left: '36px',
											width: '13.67px',
											height: '13.67px',
											marginRight: '20px',
										}}
									/>
									&nbsp; &nbsp; &nbsp; &nbsp;Facebook
								</button>
							</div>
						</div>
					</div>
				</div>
			) : null}
		</div>
	)
}

export default LogInModal
