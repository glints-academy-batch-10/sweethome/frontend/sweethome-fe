import React from "react";
import {
  TabContainer,
  NavItem,
} from "../../assets/styles/components/dashboardPage/Tab.styles";

export default function Tab(props) {
  const tabItems = [
    { name: "Appointments", nav: "appointments" },
    { name: "Projects", nav: "projects" },
    { name: "Favourites", nav: "favourites" },
  ];

  return (
    <TabContainer>
      {tabItems.map((item) => (
        <NavItem
          onClick={(e) => {
            e.preventDefault();
            props.setNavigation(item.nav);
          }}
          active={props.navigation === item.nav}
        >
          {item.name}
        </NavItem>
      ))}
    </TabContainer>
  );
}
