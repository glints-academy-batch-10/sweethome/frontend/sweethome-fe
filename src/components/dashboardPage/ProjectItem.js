import React from "react";
import { ProjectContainer } from "../../assets/styles/components/dashboardPage/ProjectItem.styles";

export default function ProjectItem(props) {
  return (
    <ProjectContainer>
      <div className="item-title">{props.title}</div>
      <div className="item-detail">{props.detail}</div>

      {/* <div className="tab-title">{props.tabtitle}</div>
      <div className="project-type">{props.type}</div>
      <div className="tab-detail">{props.tabdetail}</div> */}
    </ProjectContainer>
  );
}
