import React from "react";
import ProjectItem from "./ProjectItem";

export default function ProjectDetails() {
  return (
    <div>
      <div className="">
        Transfer to xxx. Volutpat, risus sit augue at amet orci mauris viverra.
      </div>
      <div>
        <div>
          <ProjectItem title="Project ID" detail="#P-0001" />
          <ProjectItem title="Building Type" detail="House" />
          <ProjectItem title="Related Appoinment" detail="#A001" />
          <span>24 February 2021 | 09.00 am - 10.00 pm</span>
        </div>
        <div>
          <ProjectItem
            title="Address"
            detail="Palm Beach, Pakuwon City, Surabaya"
          />
        </div>
        <div>
          <button>Upload Receipt</button>
          <span>Request cancellation</span>
        </div>
      </div>
      <div>
        <ProjectItem
          tabtitle="Project Type"
          type="Tilling Works, Carpentary Works"
        />
      </div>
      <div>
        <ProjectItem tabtitle="Area" tabdetail="Living Room" />
        <ProjectItem />
      </div>
    </div>
  );
}
