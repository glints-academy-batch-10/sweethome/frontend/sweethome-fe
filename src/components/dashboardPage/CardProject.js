import React, { useState } from "react";
import { IoIosArrowDown } from "react-icons/io";
import { CardContainer } from "../../assets/styles/components/dashboardPage/Card.styles";
import ProjectDetails from "../dashboardPage/ProjectDetails";

export default function CardProject(props) {
  console.log(props);

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <CardContainer className="card">
      <div className="top-section">
        <div className="left">
          <div className="top-label">
            <div className="label">Waiting Payment</div>
            <div className="created-date">
              Created at 20 January 2021, 9:45 PM
            </div>
          </div>
          <div className="down-label">
            <div className="date">Tilling Works</div>
            <div className="time">+ 2 packages more</div>
          </div>
        </div>

        <div className="toogle" onClick={toggle}>
          <IoIosArrowDown />
        </div>
      </div>
      {isOpen && (
        <div className="bottom-section">
          <ProjectDetails />
        </div>
      )}
    </CardContainer>
  );
}
