import React, { useState } from "react";
import { CardContainer } from "../../assets/styles/components/dashboardPage/Card.styles";
import { IoIosArrowDown } from "react-icons/io";
import AppointmentDetails from "./AppointmentDetails";

export default function Card(props) {
  console.log(props);

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <CardContainer className="card">
      <div className="top-section">
        <div className="left">
          <div className="top-label">
            <div className="label">Waiting Approval</div>
            <div className="created-date">
              Created at 20 January 2021, 9:45 PM
            </div>
          </div>
          <div className="down-label">
            <div className="date">24 February 2021 |</div>
            <div className="time">09.00 am - 10.00 pm</div>
          </div>
        </div>

        <div className="toogle" onClick={toggle}>
          <IoIosArrowDown />
        </div>
      </div>
      {isOpen && (
        <div className="bottom-section">
          <AppointmentDetails />
        </div>
      )}
    </CardContainer>
  );
}
