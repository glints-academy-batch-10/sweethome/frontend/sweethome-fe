import React from "react";
import AppoinmentItem from "./AppoinmentItem";
import {
  Detail,
  Container,
} from "../../assets/styles/components/dashboardPage/AppointmentDetails.styles";

export default function AppointmentDetails() {
  return (
    <Container>
      <div className="left">
        AppointmentDetails
        <Detail>
          <AppoinmentItem title="Building Type" data="House" />
          <AppoinmentItem title="Service Type" data="Interior Design" />
        </Detail>
        <Detail>
          <AppoinmentItem title="Estimated Work Duration" data="1 Month" />
          <AppoinmentItem title="Budget" data="Rp.1.000.000.000" />
        </Detail>
        <AppoinmentItem
          title="Address"
          data="Palm Beach, Pakuwon City, Surabaya"
        />
        <AppoinmentItem
          title="Inspection Area"
          data="Living Room and Bedroom"
        />
      </div>

      <div className="right">
        Note
        <div className="notes">
          Get free professional consultation. Volutpat, risus sit augue at amet
          orci mauris viverra. Get free professional consultation. Volutpat,
          risus sit augue at amet orci mauris viverra.
        </div>
      </div>
    </Container>
  );
}
