import styled from "styled-components";

export const UserContainer = styled.div`
  display: flex;
  flex-direction: column;

  .container {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    h3 {
      margin: 0;
      font-weight: bold;
      font-size: 24px;
      color: #000000;
    }

    p {
      margin: 0;
      font-size: 18px;
      color: #000000;
    }

    .user-detail {
      display: flex;
      flex-direction: row;

      .link {
        color: #214457;
      }
    }

    button {
      width: 283px;
      height: 56px;
      background: #214457;
      font-weight: bold;
      font-size: 16px;
      color: #ffffff;
      display: flex;
      align-items: center;
      justify-content: center;

      .plus {
        margin-right: 10px;
        font-size: 30px;
        margin-top: 4px;
      }
    }
  }

  .pict {
    margin-right: 20px;
  }
`;
