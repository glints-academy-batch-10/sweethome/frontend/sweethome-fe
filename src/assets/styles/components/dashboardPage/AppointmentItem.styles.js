import styled from "styled-components";

export const ItemContainer = styled.div`
  margin: 10px 10px 10px 0;
  .dashboard_title {
    font-size: 14px;
    color: #999999;
    width: 250px;
  }

  .dashboard_data {
    font-weight: bold;
    font-size: 14px;
    color: #373737;
  }
`;
