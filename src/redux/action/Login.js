import { LOG_IN } from '../actionTypes/LoginType'
import axios from 'axios'
import jwt_decode from 'jwt-decode'

export const login = (payload) => {
	return {
		type: LOG_IN,
		payload,
	}
}

export const postLogin = (body) => (dispatch) => {
	axios
		.post('https://sweethome-api-v1.herokuapp.com/api/v1/login', body)
		.then((response) => {
			console.log(response)
			const decoded = jwt_decode(response.data.data.token)
			console.log(decoded)
			dispatch({ type: LOG_IN, payload: decoded })
		})
		.catch((error) => {
			console.log(error)
		})
}
